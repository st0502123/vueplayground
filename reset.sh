echo Warning: this will delete all your untracked files !
echo Be sure to backup untracked files before reset!

read -p "Are you sure (y/N): " yes

if [ $yes = y ] || [ $yes = Y ] || \
	[ $yes = yes ] || [ $yes = Yes ] || [ $yes = YES ]
then
	yes=Y
else
	yes=N
fi


if [ $yes = Y ]
then
	git reset --hard
	git clean -f -d
	echo reset finished !
fi


